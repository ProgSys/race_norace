#include "csapp.h"

#define N 4

char *mensaje = "Salida del hilo X\n";
sem_t sol;

void charatatime(char * str)
{
	char * ptr;
	int c;
	/* Ensure that characters sent to stdout are output as soon
	   as possible - make stdout unbuffered. */
	setbuf(stdout,NULL);
	for(ptr = str; (c = *ptr++); ){
		usleep(1000);
		putc(c, stdout);
	}
}

void *thread(void *varg)
{
	int len = strlen(mensaje);
	char *str = Malloc(len*sizeof(char));
	int num = *((int *) varg);
	
	strcpy(str,mensaje);
	str[len - 2] = 48 + num;
	P(&sol);
	charatatime(str);
	V(&sol);
	Free(str);
	Free(varg);
	return NULL;
}

int main()
{
	pthread_t tid;
	sem_init(&sol, 0,1);
	for (int i = 0;i < N; i++){
		int * index = Malloc(sizeof(int));
		*index = i;
		Pthread_create(&tid, NULL, thread, index);
		
	}

	Pthread_exit(0);
}
